namespace ArtsukiDomain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("artsuki.event")]
    public partial class Event
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Event()
        {
            artsuki = new HashSet<Artsuki>();
        }

        public DateTime? dateEnd { get; set; }

        public DateTime? dateStart { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public int nbSeats { get; set; }

        [StringLength(255)]
        public string photo { get; set; }

        [StringLength(255)]
        public string place { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(255)]
        public string category { get; set; }

        public int etat { get; set; }

        [StringLength(255)]
        public string places { get; set; }

        [StringLength(255)]
        public string type { get; set; }

        public virtual Publication publication { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Artsuki> artsuki { get; set; }
    }
}
