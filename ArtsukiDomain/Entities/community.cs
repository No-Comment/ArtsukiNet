namespace ArtsukiDomain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("artsuki.community")]
    public partial class Community
    {
        [StringLength(255)]
        public string attachement { get; set; }

        [StringLength(255)]
        public string message { get; set; }

        [StringLength(255)]
        public string theme { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public virtual Publication publication { get; set; }
    }
}
