﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiDomain.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public virtual Artsuki Buyer { get; set; }
        public int ArtsukiId { get; set; }
        public virtual Item Item { get; set; }
        public double Amount { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
