namespace ArtsukiDomain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("artsuki.friendship")]
    public partial class Friendship
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(255)]
        public string status { get; set; }

        public int? Receiver_id { get; set; }

        public int? Requester_id { get; set; }

        public virtual Artsuki artsuki { get; set; }

        public virtual Artsuki artsuki1 { get; set; }
    }
}
