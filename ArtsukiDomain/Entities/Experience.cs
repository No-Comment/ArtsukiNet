﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiDomain.Entities
{
    public class Experience
    {
        public string Photo { get; set; }
        public int Id { get; set; }
        public int EventId { get; set; }
        public int ArtsukiId { get; set; }
        public String Comment { get; set; }
        public int Rate { get; set; }
    }
}
