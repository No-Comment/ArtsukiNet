namespace ArtsukiDomain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("artsuki.comment")]
    public partial class Comment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Comment()
        {
            comment1 = new HashSet<Comment>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(255)]
        public string content { get; set; }

        public int nbrSignla { get; set; }

        public int? commenter_id { get; set; }

        public int? publication_id { get; set; }

        public int? comment_fk { get; set; }

        public virtual Artsuki artsuki { get; set; }

        public virtual Publication publication { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> comment1 { get; set; }

        public virtual Comment comment2 { get; set; }
    }
}
