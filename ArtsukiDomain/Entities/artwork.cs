namespace ArtsukiDomain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("artsuki.artwork")]
    public partial class Artwork
    {
        [StringLength(255)]
        public string description { get; set; }

        [Column(TypeName = "bit")]
        public bool forSale { get; set; }

        [StringLength(255)]
        public string media_type { get; set; }

        public double price { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int? project_id { get; set; }

        public virtual Project project { get; set; }

        public virtual Publication publication { get; set; }
    }
}
