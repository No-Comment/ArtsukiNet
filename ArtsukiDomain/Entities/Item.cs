﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiDomain.Entities
{
    public class Item
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public string Label { get; set; }
        public string ImageName { get; set; }
        public string  Description { get; set; }
        public int ArtsukiId { get; set; }
        public virtual Artsuki Owner { get; set; }
        public virtual Order Order { get; set; }
    }
}
