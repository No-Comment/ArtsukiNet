namespace ArtsukiDomain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("artsuki.rating")]
    public partial class Rating
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public float value { get; set; }

        public int? publication_id { get; set; }

        public int? rater_id { get; set; }

        public virtual Artsuki artsuki { get; set; }

        public virtual Publication publication { get; set; }
    }
}
