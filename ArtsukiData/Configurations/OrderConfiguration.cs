﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtsukiDomain.Entities;

namespace ArtsukiData.Configurations
{
    class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            HasRequired(o => o.Buyer)
                .WithMany(a => a.Orders);

            HasRequired(o => o.Item)
                .WithOptional(i => i.Order);
        }
    }
}
