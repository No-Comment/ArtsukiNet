namespace ArtsukiData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingItemTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price = c.Double(nullable: false),
                        Label = c.String(unicode: false),
                        Description = c.String(unicode: false),
                        ArtsukiId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ArtsukiId);
            AddForeignKey("Items", "ArtsukiId", "artsuki", "id");
            
        }
        
        public override void Down()
        {
            DropForeignKey("Items", "ArtsukiId", "artsuki");
            DropIndex("Items", new[] { "ArtsukiId" });
            DropTable("Items");
        }
    }
}
