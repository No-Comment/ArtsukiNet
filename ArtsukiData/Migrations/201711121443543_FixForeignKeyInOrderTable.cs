namespace ArtsukiData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixForeignKeyInOrderTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Orders", "itemId_Id", "Items");
            //DropForeignKey("Orders", "Item_Id", "Items");
            //DropForeignKey("Items", "Order_Id", "Orders");
            //DropIndex("Items", new[] { "Order_Id" });
            //DropIndex("Orders", new[] { "Item_Id" });
            DropIndex("Orders", new[] { "itemId_Id" });
           // DropColumn("Orders", "Id");
          //  DropColumn("Orders", "Id");
           // RenameColumn(table: "Orders", name: "Order_Id", newName: "Id");
          //  RenameColumn(table: "Orders", name: "Item_Id", newName: "Id");
            //DropPrimaryKey("Orders");
            //AlterColumn("Orders", "Id", c => c.Int(nullable: false));
            //AlterColumn("Orders", "Id", c => c.Int(nullable: false));
            //AddPrimaryKey("Orders", "Id");
            //CreateIndex("Orders", "Id");
            //DropColumn("Items", "Order_Id");
            DropColumn("Orders", "itemId_Id");
        }
        
        public override void Down()
        {
            AddColumn("Orders", "itemId_Id", c => c.Int());
            //AddColumn("Items", "Order_Id", c => c.Int());
            //DropIndex("Orders", new[] { "Id" });
            //DropPrimaryKey("Orders");
            //AlterColumn("Orders", "Id", c => c.Int());
            //AlterColumn("Orders", "Id", c => c.Int(nullable: false, identity: true));
            //AddPrimaryKey("Orders", "Id");
            //RenameColumn(table: "Orders", name: "Id", newName: "Item_Id");
            //RenameColumn(table: "Orders", name: "Id", newName: "Order_Id");
            //AddColumn("Orders", "Id", c => c.Int(nullable: false, identity: true));
            //AddColumn("Orders", "Id", c => c.Int(nullable: false, identity: true));
            CreateIndex("Orders", "itemId_Id");
            //CreateIndex("Orders", "Item_Id");
            //CreateIndex("Items", "Order_Id");
            //AddForeignKey("Items", "Order_Id", "Orders", "Id");
            AddForeignKey("Orders", "itemId_Id", "Items", "Id");
        }
    }
}
