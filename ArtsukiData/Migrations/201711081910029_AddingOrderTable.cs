namespace ArtsukiData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingOrderTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ArtsukiId = c.Int(nullable: false),
                    Amount = c.Double(nullable: false),
                    Item_Id = c.Int(),
                    itemId_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Items", t => t.Item_Id)
                .Index(t => t.ArtsukiId)
                .Index(t => t.Item_Id);
            AddForeignKey("Orders", "ArtsukiId", "Artsuki", "id");
            AddColumn("Items", "Order_Id", c => c.Int());
            CreateIndex("Items", "Order_Id");
            AddForeignKey("Items", "Order_Id", "Orders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Items", "Order_Id", "Orders");
            DropForeignKey("Orders", "Item_Id", "Items");
            DropForeignKey("Orders", "ArtsukiId", "artsuki");
            DropIndex("Orders", new[] { "itemId_Id" });
            DropIndex("Orders", new[] { "Item_Id" });
            DropIndex("Orders", new[] { "ArtsukiId" });
            DropIndex("Items", new[] { "Order_Id" });
            DropColumn("Items", "Order_Id");
            DropTable("Orders");
        }
    }
}
