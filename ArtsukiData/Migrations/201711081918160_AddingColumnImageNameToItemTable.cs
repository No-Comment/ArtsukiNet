namespace ArtsukiData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingColumnImageNameToItemTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("Items", "ImageName", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("Items", "ImageName");
        }
    }
}
