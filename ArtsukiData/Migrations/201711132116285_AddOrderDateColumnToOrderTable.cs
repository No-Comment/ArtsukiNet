namespace ArtsukiData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderDateColumnToOrderTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("Orders", "OrderDate", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("Orders", "OrderDate");
        }
    }
}
