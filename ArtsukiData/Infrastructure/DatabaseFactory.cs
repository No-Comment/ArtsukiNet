﻿using ArtsukiData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiData.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private ArtsukiContext dataContext;
        public ArtsukiContext DataContext { get { return dataContext; } }

        public DatabaseFactory()
        {
            dataContext = new ArtsukiContext();
        }

        protected override void DisposeCore()
        {
            // libérer espace mémoire du context
            if(DataContext!=null)
            DataContext.Dispose();
        }
    }

}
