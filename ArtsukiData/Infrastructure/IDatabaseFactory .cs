﻿using ArtsukiData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiData.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        ArtsukiContext DataContext { get; }
    }

}
