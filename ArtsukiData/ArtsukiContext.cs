using ArtsukiData.Configurations;

namespace ArtsukiData
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using ArtsukiDomain.Entities;

    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public partial class ArtsukiContext : DbContext
    {
        public ArtsukiContext() : base("name=ArtsukiContext")
        {
            
        }

        public virtual DbSet<Artsuki> artsuki { get; set; }
        public virtual DbSet<Artwork> artwork { get; set; }
        public virtual DbSet<Comment> comment { get; set; }
        public virtual DbSet<Community> community { get; set; }
        public virtual DbSet<Event> _event { get; set; }
        public virtual DbSet<Friendship> friendship { get; set; }
        public virtual DbSet<Project> project { get; set; }
        public virtual DbSet<Publication> publication { get; set; }
        public virtual DbSet<Rating> rating { get; set; }
        public virtual DbSet<Visitor> visitor { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<Order> Order { get; set;}
		public virtual DbSet<Experience> Experience { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Artsuki>()
                .Property(e => e.art)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.biography)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.firstname)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.gender)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.lastname)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.nationality)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.phoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.role)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.street)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.town)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.publication)
                .WithOptional(e => e.artsuki)
                .HasForeignKey(e => e.publisher_id);

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.friendship)
                .WithOptional(e => e.artsuki)
                .HasForeignKey(e => e.Requester_id);

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.comment)
                .WithOptional(e => e.artsuki)
                .HasForeignKey(e => e.commenter_id);

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.project)
                .WithOptional(e => e.artsuki)
                .HasForeignKey(e => e.publisher_id);

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.friendship1)
                .WithOptional(e => e.artsuki1)
                .HasForeignKey(e => e.Receiver_id);

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.rating)
                .WithOptional(e => e.artsuki)
                .HasForeignKey(e => e.rater_id);

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.artsuki1)
                .WithMany(e => e.artsuki2)
                .Map(m => m.ToTable("artsuki_artsuki", "artsuki").MapLeftKey("Artsuki_id").MapRightKey("friends_id"));

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.project1)
                .WithMany(e => e.artsuki1)
                .Map(m => m.ToTable("artsuki_project", "artsuki").MapLeftKey("collaborators_id").MapRightKey("projectsToCollaborate_id"));

            modelBuilder.Entity<Artsuki>()
                .HasMany(e => e.project2)
                .WithMany(e => e.artsuki2)
                .Map(m => m.ToTable("project_artsuki", "artsuki").MapLeftKey("subscribers_id").MapRightKey("subscribedProjects_id"));

            modelBuilder.Entity<Artwork>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Artwork>()
                .Property(e => e.media_type)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .Property(e => e.content)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .HasMany(e => e.comment1)
                .WithOptional(e => e.comment2)
                .HasForeignKey(e => e.comment_fk);

            modelBuilder.Entity<Community>()
                .Property(e => e.attachement)
                .IsUnicode(false);

            modelBuilder.Entity<Community>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<Community>()
                .Property(e => e.theme)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.photo)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.place)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.category)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.places)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<Event>()
                .HasMany(e => e.artsuki)
                .WithMany(e => e._event)
                .Map(m => m.ToTable("artsuki_event", "artsuki").MapRightKey("participant_id"));

            modelBuilder.Entity<Friendship>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<Project>()
                .Property(e => e.project_category)
                .IsUnicode(false);

            modelBuilder.Entity<Project>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Project>()
                .Property(e => e.photo)
                .IsUnicode(false);

            modelBuilder.Entity<Project>()
                .Property(e => e.title)
                .IsUnicode(false);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.artwork)
                .WithOptional(e => e.project)
                .HasForeignKey(e => e.project_id);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.visitor)
                .WithOptional(e => e.project)
                .HasForeignKey(e => e.project_id);

            modelBuilder.Entity<Publication>()
                .Property(e => e.publicationType)
                .IsUnicode(false);

            modelBuilder.Entity<Publication>()
                .Property(e => e.title)
                .IsUnicode(false);

            modelBuilder.Entity<Publication>()
                .HasOptional(e => e.artwork)
                .WithRequired(e => e.publication);

            modelBuilder.Entity<Publication>()
                .HasMany(e => e.comment)
                .WithOptional(e => e.publication)
                .HasForeignKey(e => e.publication_id);

            modelBuilder.Entity<Publication>()
                .HasOptional(e => e.community)
                .WithRequired(e => e.publication);

            modelBuilder.Entity<Publication>()
                .HasOptional(e => e._event)
                .WithRequired(e => e.publication);

            modelBuilder.Entity<Publication>()
                .HasMany(e => e.rating)
                .WithOptional(e => e.publication)
                .HasForeignKey(e => e.publication_id);

            modelBuilder.Entity<Publication>()
                .HasMany(e => e.artsuki1)
                .WithMany(e => e.publication1)
                .Map(m => m.ToTable("artsuki_publication", "artsuki").MapLeftKey("bookmarks_id").MapRightKey("Artsuki_id"));

            modelBuilder.Entity<Visitor>()
                .Property(e => e.ipAddress)
                .IsUnicode(false);
            modelBuilder.Configurations.Add(new OrderConfiguration());

        }
    }
}
