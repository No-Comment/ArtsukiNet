﻿using ArtsukiDomain.Entities;
using ArtsukiService;
using ArtsukiWeb.Models;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace ArtsukiWeb.Controllers
{
   

    public class ArtsukiController : Controller
    {
        UserArtsukiService uas=new UserArtsukiService();
        OfferService os = new OfferService();

        private const string BASE_URI = "http://localhost:18484/tn.esprit.twin.artsuki-web/api/";
        private const string MAIL = "artsuki.com@gmail.com";
        private const string MAIL_PWD = "artsukiartsuki";

        // GET: Artsuki
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult authenticate()
        {

            return View();
        }


        [HttpPost]
        public ActionResult authenticate(ArtsukiViewModel artsukiCredentials)
        {
            try
            {
                
                var client = new RestClient(BASE_URI);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "");
                request.Resource = "authentication";
                request.AddJsonBody(artsukiCredentials);
                IRestResponse response = client.Execute(request);
                string token = response.Content;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    HttpClient client2 = new HttpClient();
                    client2.BaseAddress = new Uri(BASE_URI);
                    client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client2.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                    HttpResponseMessage response2 = client2.GetAsync("artsuki").Result;
                    if (response2.IsSuccessStatusCode)
                    {
                        ArtsukiViewModel avm = response2.Content.ReadAsAsync<ArtsukiViewModel>().Result;
                        Session["artsuki"] = avm;
                        Session["token"] = token;
                        ViewBag.result = avm;
                        return Redirect("/");
                    }

                }
                else
                {
                    ViewBag.msg = "Verify your credentials please !";
                    return View("authenticate");
                }

                return View("authenticate");

            }
            catch
            {
                return View();
            }
        }

        // GET: Artsuki/Profil/5
        public ActionResult Profil(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(BASE_URI);
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "artsuki/");
            request.Content = new StringContent("{\"id\":"+id+"}",
                                                Encoding.UTF8,
                                                "application/json");
            
            var response = client.SendAsync(request);
            Artsuki artsuki = response.Result.Content.ReadAsAsync<Artsuki>().Result;
            ViewBag.result = artsuki;
            if(artsuki.role =="ARTIST")
            { return View("ProfilArtist"); }
            return View("ProfilArtsuki");
            
        }

        // GET: Artsuki/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult confirm(string token)
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest(Method.GET);
            request.Resource = "/artsuki/register/confirm?token="+token;
            IRestResponse response = client.Execute(request);
            if(response.StatusCode==HttpStatusCode.OK)
            {
                ViewBag.result = "Your account has been verified";
                
            }
            else
            {
                ViewBag.result = "Failed to validate your account , verify the link please !";
            
            }
            return View();
        }

        // POST: Artsuki/Create
        [HttpPost]
        public ActionResult Create(ArtsukiViewModel artsuki,int referal=0)
        {
            if (referal != 0)
            {

                int oid = os.getExistentOfferByArtsukiId(referal);
                if(oid != -1)
                {
                    Offer offer = os.GetById(oid);
                    int balance = offer.balance;
                    offer.balance = balance+1;
                    os.Update(offer);
                    os.Commit();
                }
                else
                {
                    Offer offer = new Offer();
                    offer.ArtsukiId = referal;
                    offer.balance = 1;
                    os.Add(offer);
                    os.Commit();

                }

            }
            try
            {
                
                var client = new RestClient(BASE_URI);
                var request = new RestRequest(Method.POST);
                request.Resource = "/artsuki/register";
                request.AddJsonBody(artsuki);
                IRestResponse response = client.Execute(request);
                string token = response.Content;
                return RedirectToAction("authenticate");
            }
            catch
            {
                return View();
            }
        }

        // GET: Artsuki/Edit
        public ActionResult Edit()
        {

            return View(Session["artsuki"]);
        }

        // POST: Artsuki/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Artsuki/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Artsuki/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        public ActionResult Search(string keyword)
        {
            HttpClient client2 = new HttpClient();
            client2.BaseAddress = new Uri(BASE_URI);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);
            HttpResponseMessage response2 = client2.GetAsync("artsuki/search?data="+keyword).Result;
            if (response2.IsSuccessStatusCode)
            {

                string[][] avm = response2.Content.ReadAsAsync<string[][]>().Result;
                List<string[]> lst=new List<string[]>();
                for (int i = 0; i < avm.Length; i++)
                {
                    
                    string[] arr=new string[] { avm[i][0], avm[i][1], avm[i][2], avm[i][3], uas.friendShipStatus(((ArtsukiViewModel)Session["artsuki"]).id, int.Parse(avm[i][0])) };
                    lst.Add(arr);
                    
                }
                ViewBag.elements = lst;
                ViewBag.result = keyword;

            }
            else
            {
                ViewBag.elements = new List<string[]>();
                ViewBag.result = "No result";
            }
                return View();
        }

        public ActionResult friends()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(BASE_URI);
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);
            
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "friends/");
            var response = client.SendAsync(request);
            List<string[]> artsukis = response.Result.Content.ReadAsAsync<List<string[]>>().Result;

            ViewBag.artsukis = artsukis;
            return View();
        }

        public ActionResult requests()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(BASE_URI);
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Session["token"]);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "friends/invitations");
            var response = client.SendAsync(request);
            List<string[]> artsukis = response.Result.Content.ReadAsAsync<List<string[]>>().Result;
            if (artsukis != null)
            { ViewBag.artsukis = artsukis; }
            else
            {
                ViewBag.artsukis = new List<string[]>();
            }
            return View();
        }

        public ActionResult invite()
        {
            return View();
        }

        [HttpPost]
        public ActionResult invite(string type,string mail)
        {
            var fromAddress = new MailAddress(MAIL, "ARTSUKI");
            var toAddress = new MailAddress(mail, "");
             string fromPassword = MAIL_PWD;
             string subject = ((ArtsukiViewModel)Session["artsuki"]).firstname+" "+ ((ArtsukiViewModel)Session["artsuki"]).lastname+" Invites you !";
            string body = "You received an invitation from " + ((ArtsukiViewModel)Session["artsuki"]).firstname + " " + ((ArtsukiViewModel)Session["artsuki"]).lastname;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
            return View();
        }

        public ActionResult disconnect()
        {
            Session.RemoveAll();
            return Redirect("/");
        }
    }
}
