﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ArtsukiService;
using ArtsukiServices;
using ArtsukiWeb.Models;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;

namespace ArtsukiWeb.Controllers
{
    public class ProjectController : Controller
    {
        private IProjectService projectService = new ProjectService();
        private const string BASE_URI = "http://localhost:18080/tn.esprit.twin.artsuki-web/api/";
        private const string UPLOAD_PATH = "";
        private ArtsukiViewModel connectedUser;
        private IMemberService memberService = new MemberService();

        private string token;

        public void authenticate()
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest(Method.POST);
            client.ClearHandlers();
            client.AddHandler("application/json", new JsonDeserializer());
            request.RequestFormat = DataFormat.Json;
            request.Resource = "authentication/";
            var obj = new
            {
                email = "artsuki7@artsuki.com",
                password = "753357"

            };
            request.AddJsonBody(obj);
            client.AddDefaultHeader("accept", "*/*");
            var response = client.Execute(request);
            token = response.Content;
        }
        [Route("Token")]
        public ActionResult Token()
        {
            authenticate();
            return Content(token);
        }

        public ArtsukiViewModel getConnectedUser()
        {
            try
            {
                if (token == null)
                {
                    authenticate();
                }

                if (connectedUser == null)
                {
                    var client = new RestClient(BASE_URI);
                    client.AddDefaultHeader("accept", "*/*");
                    client.AddDefaultHeader("authorization", "Bearer " + token);
                    var request = new RestRequest(Method.GET);
                    request.Resource = "artsuki";
                    var response = client.Execute<ArtsukiViewModel>(request);
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        authenticate();
                        getConnectedUser();
                    }
                    connectedUser = response.Data;
                    if (connectedUser == null) throw new NullReferenceException();
                    return connectedUser;
                }
                
                return connectedUser;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        // GET: Project
        [Route("Project/Index/{page}")]
        [Route("Project/Index")]
        [Route("Project")]
        public ActionResult Index(int? page = 1)
        {
            try
            {
                //if (page == 0) page = 1; 
                var projects = projectService.GetAll();
                var client = new RestClient(BASE_URI);
                var request = new RestRequest("project");
                request.Method = Method.GET;
                request.AddParameter("page", page, ParameterType.GetOrPost);
                ViewBag.Page = page;
                request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                var response = client.Execute<List<ProjectViewModel>>(request);
                request = new RestRequest("project", Method.GET);
                request.AddParameter("numberPage", 1, ParameterType.GetOrPost);
                var NbPageResp = client.Execute<NumberPageViewModel>(request);

                if (NbPageResp.Data.number / 10 > Convert.ToInt32(NbPageResp.Data.number / 10))
                    ViewBag.TotalPage = Convert.ToInt32(NbPageResp.Data.number / 10) + 1;
                else
                    ViewBag.TotalPage = Convert.ToInt32(NbPageResp.Data.number / 10);
                ViewBag.ConnectedUser = getConnectedUser();
                ViewBag.Subscribed = getSubscribedProject();
                return View(response.Data);
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
           
        }

        // GET: Project/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var clientResult = new RestClient(BASE_URI);
                var request = new RestRequest("project/show", Method.POST);
                clientResult.AddDefaultHeader("accept", "*/*");
                var obj = new ProjectViewModel() { id = id };
                ViewBag.Connected = getConnectedUser();
                request.AddJsonBody(obj);
                var response = clientResult.Execute<ProjectViewModel>(request);
                if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.NotFound)
                {
                    return RedirectToAction("CheckConnection", "Project");
                }
                ViewBag.NbCollaborator = projectService.GetNumberCollaborators(id);
                ViewBag.NbSubscriber = projectService.GetNumberSubscribers(id);
                ViewBag.NbViews = GetNbViewsForProject(id);
                ViewBag.Connected = getConnectedUser();
                ViewBag.Subscribed = getSubscribedProject();
                return View(response.Data);

            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
        }

        public int GetNbViewsForProject(int id)
        {
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("project/views", Method.POST);
            clientResult.AddDefaultHeader("accept", "*/*");
            var obj = new ProjectViewModel() { id = id };
            request.AddJsonBody(obj);
            var response = clientResult.Execute(request);
            return Int32.Parse(response.Content);
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Project/Create
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase photo)
        {
            try
            {
                var title = Request.Form["title"];
                var description = Request.Form["description"];
                var category = Request.Form["category"];

                var photoName = Path.GetFileName(photo.FileName);
                string path = Path.Combine(Server.MapPath("~/Content/Upload/Shop"),
                                   Path.GetFileName(photo.FileName));
                photo.SaveAs(path);  
                var projectModel = new ProjectViewModel();
                projectModel.category = category;
                projectModel.title = title;
                projectModel.description = description;
                projectModel.photo = photoName;
                    //To make sure that the token is valid
                    authenticate();
                    var restClient = new RestClient(BASE_URI);
                    restClient.AddDefaultHeader("accept", "*/*");
                    restClient.AddDefaultHeader("authorization", "Bearer "+token);
                    var request = new RestRequest("project", Method.POST);
                    request.AddJsonBody(projectModel);
                    var response = restClient.Execute(request);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ViewBag.ErrorMessage = "Description is Too long";
                    return RedirectToAction("Create");
                }
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    return RedirectToAction("CheckConnection");
                }
        }

        // GET: Project/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                var restClient = new RestClient(BASE_URI);
                var request = new RestRequest("project/show", Method.POST);
                restClient.AddDefaultHeader("accept", "*/*");
                restClient.AddDefaultHeader("authorization", "Bearer " + token);
                var project = new ProjectViewModel();
                project.id = id;
                request.AddJsonBody(project);
                request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                var response = restClient.Execute<ProjectViewModel>(request);
                if (response.Data == null) return RedirectToAction("NotFound");
                if (response.Data.publisher.id == getConnectedUser().id)
                {
                    return View(response.Data);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
            
           
        }

        // POST: Project/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, HttpPostedFileBase photo)
        {
            try
            {
                // TODO: Add update logic here
                var title = Request.Form["title"];
                var description = Request.Form["description"];
                var category = Request.Form["category"];
                var photoName = Request.Form["imageNameReserve"];
                if (photo != null && photo.ContentLength > 0)
                {
                    // TODO: Add insert logic here
                    photoName = Path.GetFileName(photo.FileName);
                    string path = Path.Combine(Server.MapPath("~/Content/Upload/Shop"),
                        Path.GetFileName(photo.FileName));
                    photo.SaveAs(path);
                }

                var projectModel = new ProjectViewModel();
                projectModel.category = category;
                projectModel.title = title;
                projectModel.description = description;
                projectModel.photo = photoName;
                //To make sure that the token is valid
                authenticate();
                var restClient = new RestClient(BASE_URI);
                restClient.AddDefaultHeader("accept", "*/*");
                restClient.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("project", Method.POST);
                request.AddJsonBody(projectModel);
                var response = restClient.Execute(request);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ViewBag.ErrorMessage = "Description is Too long";
                    return RedirectToAction("Edit", id);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERROR:" + ex.Message.ToString();
                return RedirectToAction("Edit", id); 
            }
         
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                var project = new ProjectViewModel() { id = id, publisher = getConnectedUser() };
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                authenticate();
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("project", Method.DELETE);
                request.AddJsonBody(project);
                var response = client.Execute(request);
                return RedirectToAction("MyProjects");
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
            
        }

        // POST: Project/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var project = new ProjectViewModel(){ id = id};
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                authenticate();
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("project", Method.DELETE);
                request.AddJsonBody(project);
                client.Execute(request);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [Route("Project/{id}/Subscribe", Name = "Subscribe" )]
        public ActionResult Subscribe(int id)
        {
            try
            {
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("project/subscribe/add", Method.PUT);
                request.AddParameter("project_id", id, ParameterType.GetOrPost);
                request.AddHeader("authorization", "Bearer " + token);
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    authenticate();
                    return Subscribe(id);
                }
                return RedirectToAction("Index", "Project", 1);
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
          
        }

        [Route("Project/{id}/Unsubscribe")]
        public ActionResult Unsubscribe(int id)
        {
            try
            {
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("project/subscribe/remove", Method.PUT);
                request.AddParameter("project_id", id, ParameterType.GetOrPost);
                request.AddHeader("authorization", "Bearer " + token);
                var response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    authenticate();
                    return Unsubscribe(id);
                }
                return RedirectToAction("Subscribers", "Project", new {id = id});
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
            
        }

        public List<ProjectViewModel> getSubscribedProject()
        {
            
            var client = new RestClient(BASE_URI);
            client.AddDefaultHeader("accept", "*/*");
            client.AddDefaultHeader("authorization", "Bearer " + token);
            var request = new RestRequest(Method.POST);
            request.Resource = "project/subscribed";
            request.AddJsonBody(getConnectedUser());
            var response = client.Execute<List<ProjectViewModel>>(request);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                authenticate();
                getSubscribedProject();
            }
            return response.Data;
        }
        [Route("Project/Subscribed")]
        public ActionResult Subscribed()
        {
            try
            {
                var list = getSubscribedProject();
                ViewBag.Connected = getConnectedUser();
                return View(list);
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
            
        }

        [Route("Project/MyProjects")]
        public ActionResult MyProjects()
        {
            try
            {
                var restClient = new RestClient(BASE_URI);
                var request = new RestRequest("project/perUser", Method.POST);
                restClient.AddDefaultHeader("accept", "*/*");
                request.AddJsonBody(getConnectedUser());
                request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                var response = restClient.Execute<List<ProjectViewModel>>(request);
                ViewBag.Connected = getConnectedUser();
                return View(response.Data);
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
        }

        [Route("Artsuki")]
        [HttpPost]
        public ActionResult AutocompleteArtsuki(string keyword)
        {
            try
            {
                var artsuki = memberService.GetAll();
                var artuskiList = (from a in artsuki where a.email.Contains(keyword) select new { a.id, a.email });
                return Json(artuskiList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
           
        }

        [Route("Collaborator")]
        [HttpPost]
        public ActionResult AddCollaborator()
        {
            try
            {
                 var artsukiId = Request.Form["user_id"];
                            var artsuki = new ArtsukiViewModel()
                            {
                                id = Int32.Parse(artsukiId)
                            };
                            var restClient = new RestClient(BASE_URI);
                            var request = new RestRequest("/project/collaborators/add", Method.PUT);
                            restClient.AddDefaultHeader("accept", "*/*");
                            getConnectedUser();
                            restClient.AddDefaultHeader("authorization", "Bearer "+token);
                            request.AddParameter("projectId", Int32.Parse(Request.Form["projectId"]), ParameterType.GetOrPost);
                            request.AddParameter("artsukiId", artsuki.id, ParameterType.GetOrPost);
                            var response = restClient.Execute(request);
                            return RedirectToAction("Collaborators", new { id = Int32.Parse(Request.Form["projectId"]) });
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
           
        }

        [Route("Project/Scroll/{page}")]
        public ActionResult ScrollProject(int page)
        {
            try
            {
             var client = new RestClient(BASE_URI);
                        var request = new RestRequest("project");
                        request.AddParameter("page", page, ParameterType.GetOrPost);
                        request.Method = Method.GET;
                        request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                        var response = client.Execute<List<ProjectViewModel>>(request);
                        ViewBag.ConnectedUser = getConnectedUser();
                        ViewBag.Subscribed = getSubscribedProject();
                        return PartialView(response.Data);
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
           
        }
        [Route("CheckConnection")]
        public ActionResult CheckConnection()
        {
            return View();
        }
        [Route("Project/{id}/Collaborators")]
        public ActionResult Collaborators(int id)
        {
            var client = new RestClient(BASE_URI);
            client.AddDefaultHeader("accept", "*/*");
            var request = new RestRequest(Method.POST);
            request.Resource = "project/collaborators";
            request.AddJsonBody(new ProjectViewModel(){id = id});
            var response = client.Execute<List<ArtsukiViewModel>>(request);
            var collaborators = response.Data;
            request = new RestRequest("project/show", Method.POST);
            client.AddDefaultHeader("accept", "*/*");
            request.AddJsonBody(new ProjectViewModel() { id = id });
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var projectResponse = client.Execute<ProjectViewModel>(request);
            ViewBag.Project = projectResponse.Data;
            ViewBag.Connected = getConnectedUser();
            if (projectResponse.Data == null) return RedirectToAction("NotFound");
            return View(collaborators);
        }
        [Route("Project/{id}/Subscribers")]
        public ActionResult Subscribers(int id)
        {
            var client = new RestClient(BASE_URI);
            client.AddDefaultHeader("accept", "*/*");
            var request = new RestRequest(Method.POST);
            request.Resource = "project/subscribers";
            request.AddJsonBody(new ProjectViewModel() { id = id });
            var response = client.Execute<List<ArtsukiViewModel>>(request);
            var collaborators = response.Data;
            request = new RestRequest("project/show", Method.POST);
            client.AddDefaultHeader("accept", "*/*");
            request.AddJsonBody(new ProjectViewModel() { id = id });
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var projectResponse = client.Execute<ProjectViewModel>(request);
            ViewBag.Project = projectResponse.Data;
            if (projectResponse.Data == null) return RedirectToAction("NotFound");
            ViewBag.Connected = getConnectedUser();
            return View(collaborators);
        }

        public ActionResult NotFound()
        {
            return View();
        }
        [HttpPost]
        [Route("Project/Collaborator/remove")]
        public ActionResult RemoveCollaborator()
        {
            var artsukiId = Int32.Parse(Request.Form["artsukiId"]);
            var projectId = Int32.Parse(Request.Form["projectId"]);
            try
            {
                authenticate();
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("project/collaborators/remove", Method.PUT);
                request.AddParameter("projectId", projectId, ParameterType.GetOrPost);
                request.AddParameter("artsukiId", artsukiId, ParameterType.GetOrPost);
                request.AddHeader("authorization", "Bearer " + token);
                var response = client.Execute(request);
                return RedirectToAction("Collaborators", "Project", new {id = projectId});
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }

        }
        [Route("Project/Search")]
        public ActionResult SearchByKeyword(string keyword)
        {
            try
            {
                //if (page == 0) page = 1; 
                var projects = projectService.GetAll();
                var client = new RestClient(BASE_URI);
                var request = new RestRequest("project");
                request.Method = Method.GET;
                request.AddParameter("keyword", keyword, ParameterType.GetOrPost);
                request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                var response = client.Execute<List<ProjectViewModel>>(request);
                ViewBag.ConnectedUser = getConnectedUser();
                ViewBag.Subscribed = getSubscribedProject();
                return View(response.Data);
            }
            catch (Exception e)
            {
                return RedirectToAction("CheckConnection", "Project");
            }
        }
    }
}
