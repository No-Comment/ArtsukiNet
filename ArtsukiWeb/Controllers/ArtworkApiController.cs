﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Stripe;
using ArtsukiService;

namespace ArtsukiWeb.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ArtworkApiController : ApiController
    {
        private ArtworkService artworkService;

        // POST: api/ArtworkApi
        public HttpResponseMessage Charge(int artworkId, int price, string stripeEmail, string stripeToken)
        {
            try
            { var customers = new StripeCustomerService();
            var charges = new StripeChargeService();
            var amount = price;

            var customer = customers.Create(new StripeCustomerCreateOptions
            {
                Email = stripeEmail,
                SourceToken = stripeToken
            });

            var charge = charges.Create(new StripeChargeCreateOptions
            {
                Amount = amount * 100,
                Description = "Sample Charge",
                Currency = "usd",
                CustomerId = customer.Id
            });
            // further application specific code goes here
            var id = artworkId;
            artworkService.updateArtworkToSold(id);
            return Request.CreateResponse(HttpStatusCode.Created);
        }
        catch (Exception ex)
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
        }
}
    }
}
