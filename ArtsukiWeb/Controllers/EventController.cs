﻿using ArtsukiDomain.Entities;
using ArtsukiService;
using ArtsukiServices;
using ArtsukiWeb.Models;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ArtsukiWeb.Controllers
{
    public class EventController : Controller
    {
        private EventService eventService = new EventService();
        private ExperienceService expService = new ExperienceService();

        private const string BASE_URI = "http://localhost:18080/tn.esprit.twin.artsuki-web/api/";
        private const string UPLOAD_PATH = "";
        private ArtsukiViewModel connectedUser;
        private string token;

        public void authenticate()
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest(Method.POST);
            client.ClearHandlers();
            client.AddHandler("application/json", new JsonDeserializer());
            request.RequestFormat = DataFormat.Json;
            request.Resource = "authentication/";
            var obj = new
            {
                email = "artsuki7@artsuki.com",
                password = "753357"

            };
            request.AddJsonBody(obj);
            client.AddDefaultHeader("accept", "*/*");
            var response = client.Execute(request);
            token = response.Content;
        }
        [Route("Token")]
        public ActionResult Token()
        {
            authenticate();
            return Content(token);
        }

        public ArtsukiViewModel getConnectedUser()
        {
            if (token == null)
            {
                authenticate();
            }

            if (connectedUser == null)
            {
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest(Method.GET);
                request.Resource = "artsuki";
                var response = client.Execute<ArtsukiViewModel>(request);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    authenticate();
                    getConnectedUser();
                }
                connectedUser = response.Data;
                return connectedUser;
            }
            return connectedUser;
        }




        // GET: Event
        public ActionResult Index()
        {
            var events = eventService.GetAll();
            var client = new RestClient(BASE_URI);
            var request = new RestRequest("event/all");
            request.Method = Method.GET;
            Archive();
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = client.Execute<List<EventViewModel>>(request);
            // ViewBag.ConnectedUser = getConnectedUser();

            return View(response.Data);
        }
        public void Archive()
        {
            var events = new EventViewModel();
            var client = new RestClient(BASE_URI);
            client.AddDefaultHeader("accept", "*/*");
            var request = new RestRequest("event/archive", Method.PUT);
            request.AddJsonBody(events);
            client.Execute(request);


        }


        public ActionResult Details(int id)
        {            
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("event/show", Method.POST);
            clientResult.AddDefaultHeader("accept", "*/*");
            var obj = new EventViewModel() { id = id };
            ViewBag.Connected = getConnectedUser();
            Archive();
            request.AddJsonBody(obj);
            var response = clientResult.Execute<EventViewModel>(request);
            
            
            List<Experience> CommentList = new List<Experience>();
            foreach (var item in expService.getExperienceEvent(id))
            {
                Experience f = new Experience();
                f.Id = item.Id;
                f.Comment = item.Comment;
                CommentList.Add(f);
            }
            ViewBag.Comments = CommentList;
            ViewBag.Rating = expService.AvgEvent(id);

            return View(response.Data);
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            // EventViewModel om = new EventViewModel();
            return View();
        }
        // POST: Events/Create
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase photo)
        {
            try
            {
                var title = Request.Form["title"];
                var description = Request.Form["description"];
                var category = Request.Form["category"];
                var type = Request.Form["type"];
                var place = Request.Form["place"];
                var nbseats = Request.Form["nbSeat"];
                DateTime dateStart = Convert.ToDateTime(Request.Form["dateStart"]).Date;
                DateTime dateEnd = Convert.ToDateTime(Request.Form["dateEnd"]).Date;
                var photoName = Path.GetFileName(photo.FileName);
                string path = Path.Combine(Server.MapPath("~/Content/Upload/Event"),
                Path.GetFileName(photo.FileName));
                photo.SaveAs(path);
                var events = new EventViewModel();
                
                events.category = category;
                events.description = description;
                events.title = title;
                events.places = place;
                events.type = type;
                events.photo = photoName;
                events.nbSeats = Int32.Parse(nbseats);
                events.etat = 1;
                events.dateEnd =  dateEnd.ToString("yyyy-MM-ddTHH:mm:ssZ");
                events.dateStart = dateStart.ToString("yyyy-MM-ddTHH:mm:ssZ");
                //To make sure that the token is valid
                authenticate();

                var restClient = new RestClient(BASE_URI);
                restClient.AddDefaultHeader("accept", "*/*");
                restClient.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("event", Method.POST);
                request.AddJsonBody(events);
                var response = restClient.Execute(request);
                if (response.StatusCode != HttpStatusCode.Created)
                {
                    ViewBag.ErrorMessage = "Description is Too long";
                    return RedirectToAction("Create");
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERROR:" + ex.Message.ToString();
                return RedirectToAction("Create");
            }

        }
        // GET: Events/Edit/5
        public ActionResult Edit(int id)
        {
            var restClient = new RestClient(BASE_URI);
            var request = new RestRequest("event/show", Method.POST);
            restClient.AddDefaultHeader("accept", "*/*");
            restClient.AddDefaultHeader("authorization", "Bearer " + token);
            var events = new EventViewModel() { id = id };

            request.AddJsonBody(events);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = restClient.Execute<EventViewModel>(request);

            return View(response.Data);


        }

        // POST: Events/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, HttpPostedFileBase photo)
        {
            try
            {
                var title = Request.Form["title"];
                var description = Request.Form["description"];
                var category = Request.Form["category"];
                var type = Request.Form["type"];
                var place = Request.Form["place"];
                var photoName = Request.Form["photo"];
                if (photo != null && photo.ContentLength > 0)
                {
                    // TODO: Add insert logic here
                    photoName = Path.GetFileName(photo.FileName);
                    string path = Path.Combine(Server.MapPath("~/Content/Upload/Event"),
                        Path.GetFileName(photo.FileName));
                    photo.SaveAs(path);
                }

                var events = new EventViewModel() { id = id };
                events.category = category;
                events.description = description;
                events.title = title;
                events.places = place;
                events.type = type;
                events.photo = photoName;
                //To make sure that the token is valid
                authenticate();
                var restClient = new RestClient(BASE_URI);
                restClient.AddDefaultHeader("accept", "*/*");
                restClient.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("event", Method.POST);
                request.AddJsonBody(events);
                var response = restClient.Execute(request);
                if (response.StatusCode != HttpStatusCode.Created)
                {
                    ViewBag.ErrorMessage = "Description is Too long";
                    return RedirectToAction("Edit", id);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERROR:" + ex.Message.ToString();
                return RedirectToAction("Edit", id);
            }
        }

        // GET: Events/Delete/5
        public ActionResult Delete(int id)
        {
            var events = new EventViewModel() { id = id, publisher = getConnectedUser() };
            var client = new RestClient(BASE_URI);
            client.AddDefaultHeader("accept", "*/*");
            authenticate(); 
            client.AddDefaultHeader("authorization", "Bearer " + token);
            var request = new RestRequest("event", Method.DELETE);
            request.AddJsonBody(events);
            var response = client.Execute(request);
            return RedirectToAction("Index");
        }
        // POST: Events/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var project = new EventViewModel() { id = id };
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                authenticate(); 
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("event", Method.DELETE);
                request.AddJsonBody(project);
                client.Execute(request);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Upload()
        {
            return View();
        }


        [Route("Event/Details/{id}/Participate")]
        public ActionResult Participate(int id)
        {
            var client = new RestClient(BASE_URI);
            client.AddDefaultHeader("accept", "*/*");
            client.AddDefaultHeader("authorization", "Bearer " + token);
            var request = new RestRequest("event/part", Method.PUT);
            request.AddParameter("eventID", id, ParameterType.GetOrPost);
            request.AddHeader("authorization", "Bearer " + token);
            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                ViewBag.Connected = getConnectedUser();
                authenticate();
                return Participate(id);
            }
            return RedirectToAction("Details/"+id);
        }

        public EventViewModel reche(int id)
        {
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("event/show", Method.POST);
            clientResult.AddDefaultHeader("accept", "*/*");
            var obj = new EventViewModel() { id = id };
            
            request.AddJsonBody(obj);
            var response = clientResult.Execute<EventViewModel>(request);
            return obj;
        }

        [HttpPost]
        public ActionResult Upload(List<HttpPostedFileBase> photos)
        {
             var id = Request.Form["idevent"];
            var comment = Request.Form["description"];

            var artsuki = Request.Form["idart"];
            foreach (var photo in photos)
            {
                Experience o = new Experience()
                {
                    ArtsukiId= Int32.Parse(artsuki),               
                    EventId= Int32.Parse(id),
                    Photo = photo.FileName
                };
                expService.Add(o);
                expService.Commit();
               
                string path = Path.Combine(Server.MapPath("~/Content/Upload/Event"),
                    Path.GetFileName(photo.FileName));
                photo.SaveAs(path);
            }
            if (comment.Equals(""))
            {
            }
            else {
            Experience co = new Experience()
            {
                ArtsukiId =Int32.Parse(artsuki),
                EventId = Int32.Parse(id),
             
                 Comment =comment
             
            };
            expService.Add(co);
            expService.Commit();
 }
            return RedirectToAction("ExperienceView/"+id);
        }
       
        [HttpPost]       
        public ActionResult AddRate()
        {
            var id = Request.Form["idevents"];
            var artsuki = Request.Form["idarts"];
            var value = Request.Form["star"].ToString();

            Experience co = new Experience()
            {
                ArtsukiId = Int32.Parse(artsuki),
                EventId = Int32.Parse(id),
                Rate = Int32.Parse(value)
            };
            expService.Add(co);
            expService.Commit();
            return RedirectToAction("Details/"+id);
        }


        public ActionResult ExperienceView(int id)
        {

            List<ExperienceViewModel> ImageList = new List<ExperienceViewModel>();

            foreach (var item in expService.getImageEvent(id))
            {
                ExperienceViewModel f = new ExperienceViewModel();
                f.Photo = item.Photo;
                ImageList.Add(f);
            }
            return View(ImageList);
        }
        public ActionResult DeleteComment(int id)
        {
            //   Gallery a = new Gallery();
            expService.Delete(expService.GetById(id));
            Experience a = expService.GetById(id);
            
            expService.Commit();
            return RedirectToAction("Details/"+ a.EventId);
        }
       ExperienceViewModel ee= new ExperienceViewModel();
        public ActionResult EditComment(int id)
        {
            Experience a = expService.GetById(id);
            ee.Comment = a.Comment;
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult EditComment(int id, ExperienceViewModel ee)
        {
            Experience a = expService.GetById(id);
         
            
            a.Comment= ee.Comment;
            expService.Update(a);
            expService.Commit();
            return RedirectToAction("Index");

        }

    }
}
