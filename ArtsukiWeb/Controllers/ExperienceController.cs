﻿using ArtsukiDomain.Entities;
using ArtsukiService;
using ArtsukiWeb.Models;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ArtsukiWeb.Controllers
{
    public class ExperienceController : Controller
    {
        private ExperienceService expService = new ExperienceService();
        private const string BASE_URI = "http://localhost:18080/tn.esprit.twin.artsuki-web/api/";
        private const string UPLOAD_PATH = "";
        private ArtsukiViewModel connectedUser;
        private string token;
        public void authenticate()
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest(Method.POST);
            client.ClearHandlers();
            client.AddHandler("application/json", new JsonDeserializer());
            request.RequestFormat = DataFormat.Json;
            request.Resource = "authentication/";
            var obj = new
            {
                email = "artsuki5@artsuki.com",
                password = "753357"

            };
            request.AddJsonBody(obj);
            client.AddDefaultHeader("accept", "*/*");
            var response = client.Execute(request);
            token = response.Content;
        }
        [Route("Token")]
        public ActionResult Token()
        {
            authenticate();
            return Content(token);
        }

        public ArtsukiViewModel getConnectedUser()
        {
            if (token == null)
            {
                authenticate();
            }

            if (connectedUser == null)
            {
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest(Method.GET);
                request.Resource = "artsuki";
                var response = client.Execute<ArtsukiViewModel>(request);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    authenticate();
                    getConnectedUser();
                }
                connectedUser = response.Data;
                return connectedUser;
            }
            return connectedUser;
        }
        // GET: Experience
        public ActionResult Index()
        {
            return View();
        }

        // GET: Experience/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Experience/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Experience/Create
        [HttpPost]
        public ActionResult Create(List<HttpPostedFileBase> photos)
        {
            foreach(var photo in photos) { 
    
            Experience o = new Experience()
            {
              
                Photo = photo.FileName
            };

            expService.Add(o);
            expService.Commit();
}
            return RedirectToAction("Event/Index");
            
           

        
            /*foreach (var file in myFiles)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                    file.SaveAs(Server.MapPath("~/Content/Upload/Event" + file.FileName));

                    Experience exp = new Experience()
                        {
                            Photo = file.FileName,
                            
                        };
                        expService.Add(exp);
                        expService.Commit();
                        
                    }
                }*/
              

            
        }

            // GET: Experience/Edit/5
            public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Experience/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Experience/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Experience/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
