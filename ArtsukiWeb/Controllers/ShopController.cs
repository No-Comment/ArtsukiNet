﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ArtsukiDomain.Entities;
using ArtsukiService;
using ArtsukiWeb.Models;
using RestSharp;
using RestSharp.Deserializers;

namespace ArtsukiWeb.Controllers
{
    public class ShopController : Controller
    {
        private IItemService itemService = new ItemService();
        private IOrderService orderService = new OrderService();

        private ArtsukiViewModel connectedUser;
        private IMemberService memberService = new MemberService();
        private const string BASE_URI = "http://localhost:18080/tn.esprit.twin.artsuki-web/api/";
        private static string UPLOAD_PATH = "~/Content/Upload/Shop/";

        private string token;

        public void authenticate()
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest(Method.POST);
            client.ClearHandlers();
            client.AddHandler("application/json", new JsonDeserializer());
            request.RequestFormat = DataFormat.Json;
            request.Resource = "authentication/";
            var obj = new
            {
                email = "artsuki7@artsuki.com",
                password = "753357"

            };
            request.AddJsonBody(obj);
            client.AddDefaultHeader("accept", "*/*");
            var response = client.Execute(request);
            token = response.Content;
        }
        [Route("Token")]
        public ActionResult Token()
        {
            authenticate();
            return Content(token);
        }

        public ArtsukiViewModel getConnectedUser()
        {
            if (token == null)
            {
                authenticate();
            }

            if (connectedUser == null)
            {
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest(Method.GET);
                request.Resource = "artsuki";
                var response = client.Execute<ArtsukiViewModel>(request);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    authenticate();
                    getConnectedUser();
                }
                connectedUser = response.Data;
                return connectedUser;
            }
            return connectedUser;
        }

        // GET: Shop
        public ActionResult Index()
        {
            ViewBag.UploadPath = Url.Content(UPLOAD_PATH);
            var Items = itemService.GetItems();
            return View(Items);
        }

        // GET: Shop/Details/5
        public ActionResult Details(int id)
        {
            Item itemToDisplay = itemService.Get(i => i.Id == id);
            var Owner = memberService.Get(a => a.id == itemToDisplay.ArtsukiId);
            ViewBag.UploadPath = Url.Content(UPLOAD_PATH);
            ViewBag.Connected = getConnectedUser();
            ViewBag.Owner = Owner;
            return View(itemToDisplay);
        }

        // GET: Shop/Create
        public ActionResult Create()
        { 
            return View();
        }

        // POST: Shop/Create
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase Image)
        {
                
                // TODO: Add insert logic here
                var label = Request.Form["label"];
                var description = Request.Form["description"];
                var price = Request.Form["price"];
                var imageName = Path.GetFileName(Image.FileName);
                string path = Path.Combine(Server.MapPath(UPLOAD_PATH), imageName);
                Image.SaveAs(path);
                ArtsukiViewModel connected = getConnectedUser();
                var artsukiConnected = memberService.Get(a => a.id == connected.id);
                Item item = new Item()
                {
                    Label = label,
                    Description = description,
                    ImageName = imageName,
                    Price = Double.Parse(price),
                    ArtsukiId = artsukiConnected.id
                };
                itemService.Add(item);
                itemService.Commit();
                return RedirectToAction("Index");
            
        }

        // GET: Shop/Edit/5
        public ActionResult Edit(int id)
        {
            Item itemFetchedToModify = itemService.Get(i => i.Id == id);
            return View(itemFetchedToModify);
        }

        // POST: Shop/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, HttpPostedFileBase photo)
        {
            try
            {
                // TODO: Add update logic here
                var label = Request.Form["label"];
                var description = Request.Form["description"];
                var price = Request.Form["price"];
                var photoName = Request.Form["imageNameReserve"];
                if (photo != null && photo.ContentLength > 0)
                {
                    // TODO: Add insert logic here
                    photoName = Path.GetFileName(photo.FileName);
                    string path = Path.Combine(Server.MapPath(UPLOAD_PATH),photoName);
                    photo.SaveAs(path);
                }
                Item itemFetchedToModify = itemService.Get(i => i.Id == id);
                itemFetchedToModify.Label = label;
                itemFetchedToModify.Description = description;
                itemFetchedToModify.ImageName = photoName;
                itemFetchedToModify.Price = Double.Parse(price);
                itemService.Update(itemFetchedToModify);
                itemService.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Shop/Delete/5
        public ActionResult Delete(int id)
        {
            Item itemFetchedToDelete = itemService.Get(i => i.Id == id);
            itemService.Delete(itemFetchedToDelete);
            itemService.Commit();
            return RedirectToAction("Index");
        }

        [Route("Item/{id}/Buy")]
        public ActionResult Buy(int id)
        {
            Item itemToBuy = itemService.Get(i => i.Id == id);
            if (itemToBuy.Order != null)
            {
                return RedirectToAction("Index");
            }
            var connectedUser = getConnectedUser();
            if (itemToBuy.ArtsukiId == connectedUser.id)
            {
                ViewBag.message = "You Can't Buy Your Own Product";
                return RedirectToAction("Index");  
            }
            else
            {
                orderService.AddOrder(connectedUser.id, id);
                return RedirectToAction("Index");
            }
        }

        [Route("Item/MyItems")]
        public ActionResult MyItems()
        {
            var connected = getConnectedUser();
            ViewBag.UploadPath = Url.Content(UPLOAD_PATH);
            var myItems = itemService.GetMany(i => i.ArtsukiId == connected.id).ToList();
            return View(myItems);
        }

        [Route("Orders/MyOrders")]
        public ActionResult MyOrders()
        {
            var connected = getConnectedUser();
            ViewBag.UploadPath = Url.Content(UPLOAD_PATH);
            var myOrders = orderService.GetMany(o => o.ArtsukiId == connected.id).ToList();
            return View(myOrders);
        }

    }
}
