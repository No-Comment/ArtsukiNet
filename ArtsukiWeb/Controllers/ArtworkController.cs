﻿using ArtsukiWeb.Models;
using RestSharp;
using RestSharp.Deserializers;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using ArtsukiService;

namespace ArtsukiWeb.Controllers
{
    public class ArtworkController : Controller
    {
        private const string BASE_URI = "http://localhost:18080/tn.esprit.twin.artsuki-web/api/";
        private const string UPLOAD_PATH = "";
        private ArtsukiViewModel connectedUser;
        private IArtworkService artworkService = new ArtworkService();
        private string token;

        public void authenticate()
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest(Method.POST);
            client.ClearHandlers();
            client.AddHandler("application/json", new JsonDeserializer());
            request.RequestFormat = DataFormat.Json;
            request.Resource = "authentication/";
            var obj = new
            {
                email = "artsuki9@artsuki.com",
                password = "753357"

            };
            request.AddJsonBody(obj);
            client.AddDefaultHeader("accept", "*/*");
            var response = client.Execute(request);
            token = response.Content;
        }
        [Route("Token")]
        public ActionResult Token()
        {
            authenticate();
            return Content(token);
        }

        public ArtsukiViewModel getConnectedUser()
        {
            if (token == null)
            {
                authenticate();
            }

            if (connectedUser == null)
            {
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest(Method.GET);
                request.Resource = "artsuki";
                var response = client.Execute<ArtsukiViewModel>(request);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    authenticate();
                    getConnectedUser();
                }
                connectedUser = response.Data;
                return connectedUser;
            }
            return connectedUser;
        }

        // GET: Artwork
        public ActionResult Index()
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest("artwork/main");
            request.Method = Method.GET;
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = client.Execute<List<ArtworkViewModel>>(request);
            ViewBag.ConnectedUser = getConnectedUser();
            return View(response.Data);
        }

        // GET: Artwork/Details/5
        public ActionResult Details(int id)
        {
            //Stripe Configuration
            var stripePublishKey = "pk_test_B8rIDwoPvBxtx66i7Ex5mYPH";
            ViewBag.StripePublishKey = stripePublishKey;
            //WS configuration
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("artwork/all", Method.GET);
            request.AddParameter("id", id, ParameterType.GetOrPost);
            clientResult.AddDefaultHeader("accept", "*/*");
            var obj = new ArtworkViewModel() { id = id };
            ViewBag.Connected = getConnectedUser();
            request.AddJsonBody(obj);
            var response = clientResult.Execute<ArtworkViewModel>(request);
            return View(response.Data);
        }

        // GET: Artwork/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Artwork/Create
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase photo)
        {
            try
            {
                var title = Request.Form["title"];
                var description = Request.Form["description"];
                var category = Request.Form["category"];
                var price = double.Parse(Request.Form["price"]);
                var photoName = Path.GetFileName(photo.FileName);
                string path = Path.Combine(Server.MapPath("~/Content/Upload/Portfolio"), Path.GetFileName(photo.FileName));
                photo.SaveAs(path);

                var artworkModel = new ArtworkViewModel();
                artworkModel.category = category;
                artworkModel.title = title;
                artworkModel.description = description;
                artworkModel.mediaPath = photoName;
                artworkModel.price = price;
                artworkModel.publicationType = "Artwork";
                //To make sure that the token is valid
                authenticate();
                var restClient = new RestClient(BASE_URI);
                restClient.AddDefaultHeader("accept", "*/*");
                restClient.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("artwork", Method.POST);
                request.AddJsonBody(artworkModel);
                var response = restClient.Execute(request);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ViewBag.ErrorMessage = "Description is Too long";
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERROR:" + ex.Message.ToString();
                return RedirectToAction("Create");
            }

        }

        // GET: Artwork/Edit/5
        public ActionResult Edit(int id)
        {
            var restClient = new RestClient(BASE_URI);
            var request = new RestRequest("artwork/all", Method.GET);
            request.AddParameter("id", id, ParameterType.GetOrPost);
            restClient.AddDefaultHeader("accept", "*/*");
            var obj = new ArtworkViewModel() { id = id };
            ViewBag.Connected = getConnectedUser();
            request.AddJsonBody(obj);
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = restClient.Execute<ArtworkViewModel>(request);
            var connected = getConnectedUser();
            if (response.Data.publisher.id == connected.id)
            {
                return View(response.Data);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // POST: Artwork/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, HttpPostedFileBase photo)
        {
            // var id = int.Parse(Request.Form["id"]);
            try
            {
                // TODO: Add update logic here
                var title = Request.Form["title"];
                var description = Request.Form["description"];
                var category = Request.Form["category"];
                var price = Double.Parse(Request.Form["price"]);
                var photoName = Request.Form["imageNameReserve"];
                if (photo != null && photo.ContentLength > 0)
                {
                    // TODO: Add insert logic here
                    photoName = Path.GetFileName(photo.FileName);
                    string path = Path.Combine(Server.MapPath("~/Content/Upload/Portfolio"), Path.GetFileName(photo.FileName));
                    photo.SaveAs(path);
                }

                var artworkModel = new ArtworkViewModel();
                artworkModel.id = id;
                artworkModel.title = title;
                artworkModel.description = description;
                artworkModel.category = category;
                artworkModel.price = price;
                artworkModel.mediaPath = photoName;
                //To make sure that the token is valid
                authenticate();
                var restClient = new RestClient(BASE_URI);
                restClient.AddDefaultHeader("accept", "*/*");
                restClient.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("artwork/put", Method.PUT);
                request.AddJsonBody(artworkModel);
                var response = restClient.Execute(request);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ViewBag.ErrorMessage = "Description is Too long";
                    return RedirectToAction("Edit", id);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERROR:" + ex.Message.ToString();
                return RedirectToAction("Edit", id);
            }

        }

        // GET: Artwork/Delete/5
        public ActionResult Delete(int id)
        {
            var artwork = new ArtworkViewModel() { id = id, publisher = getConnectedUser() };
            var client = new RestClient(BASE_URI);
            client.AddDefaultHeader("accept", "*/*");
            authenticate();
            client.AddDefaultHeader("authorization", "Bearer " + token);
            var request = new RestRequest("artwork/delete", Method.DELETE);
            request.AddJsonBody(artwork);
            var response = client.Execute(request);
            return RedirectToAction("Index");
        }

        // POST: Artwork/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var artwork = new ArtworkViewModel() { id = id };
                var client = new RestClient(BASE_URI);
                client.AddDefaultHeader("accept", "*/*");
                authenticate();
                client.AddDefaultHeader("authorization", "Bearer " + token);
                var request = new RestRequest("artwork/delete", Method.DELETE);
                request.AddJsonBody(artwork);
                client.Execute(request);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
        // GET: Artwork?category
        public ActionResult Criteria()
        {
            var category = Request.Form["category"];
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("artwork", Method.GET);
            request.AddParameter("category", category, ParameterType.GetOrPost);
            //request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            clientResult.AddDefaultHeader("accept", "*/*");
            var obj = new ArtworkViewModel() { category = category };
            ViewBag.Connected = getConnectedUser();
            request.AddJsonBody(obj);
            var response = clientResult.Execute<List<ArtworkViewModel>>(request);
            ViewBag.ConnectedUser = getConnectedUser();
            return View(response.Data);
        }

        // GET: Artwork?minPrice&maxPrice
        public ActionResult Filter()
        {
            var minPrice = Double.Parse(Request.Form["minPrice"]);
            var maxPrice = Double.Parse(Request.Form["maxPrice"]);
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("artwork/filter", Method.GET);
            request.AddParameter("minPrice", minPrice, ParameterType.GetOrPost);
            request.AddParameter("maxPrice", maxPrice, ParameterType.GetOrPost);
            clientResult.AddDefaultHeader("accept", "*/*");
            var obj = new ArtworkViewModel();
            ViewBag.Connected = getConnectedUser();
            request.AddJsonBody(obj);
            var response = clientResult.Execute<List<ArtworkViewModel>>(request);
            ViewBag.ConnectedUser = getConnectedUser();
            return View(response.Data);
        }


        public ActionResult Charge(string stripeEmail, string stripeToken)
        {
            var customers = new StripeCustomerService();
            var charges = new StripeChargeService();
            var amount = Int32.Parse(Request.Form["amount"]);

            var customer = customers.Create(new StripeCustomerCreateOptions
            {
                Email = stripeEmail,
                SourceToken = stripeToken
            });

            var charge = charges.Create(new StripeChargeCreateOptions
            {
                Amount = amount * 100,
                Description = "Sample Charge",
                Currency = "usd",
                CustomerId = customer.Id
            });
            // further application specific code goes here
            var id = Int32.Parse(Request.Form["artworkId"]);
            artworkService.updateArtworkToSold(id);
            return RedirectToAction("Index");
        }

        // GET: Artwork?publisher
        public ActionResult MyArtworks()
        {
            var owner = int.Parse(Request.Form["owner"]);
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("artwork/publisher", Method.GET);
            request.AddParameter("idPublisher", owner, ParameterType.GetOrPost);
            clientResult.AddDefaultHeader("accept", "*/*");
            var artist = getConnectedUser();
            var obj = new ArtworkViewModel() { publisher = artist };
            ViewBag.Connected = getConnectedUser();
            request.AddJsonBody(obj);
            var response = clientResult.Execute<List<ArtworkViewModel>>(request);
            ViewBag.ConnectedUser = getConnectedUser();
            return View(response.Data);
        }

    }
}
