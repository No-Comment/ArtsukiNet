﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using ArtsukiWeb.Models;
using RestSharp;
using ArtsukiDomain.Entities;
using ArtsukiService;
using System.Web.Mvc;
using System.Web.Http.Cors;

namespace ArtsukiWeb.Controllers
{



    [EnableCors("*", "*", "*")]
    public class EventAPIController : ApiController
    {
        private const string BASE_URI = "http://localhost:18080/tn.esprit.twin.artsuki-web/api/";
        private const string UPLOAD_PATH = "";
        private ArtsukiViewModel connectedUser;
        private string token;
        private EventService eventService = new EventService();
        private EventController eventC = new EventController();
        private ExperienceService expService = new ExperienceService();

        // GET: api/EventAPI
        [System.Web.Mvc.HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        // GET: api/EventAPI/5
        public List<Experience> Get(int id)
        {
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("event/show", Method.POST);
            clientResult.AddDefaultHeader("accept", "*/*");
          
            var obj = new EventViewModel() { id = id };

            request.AddJsonBody(obj);
            var response = clientResult.Execute<EventViewModel>(request);


            List<Experience> CommentList = new List<Experience>();
            foreach (var item in expService.getExperienceEvent(id))
            {
                Experience f = new Experience();
                f.Id = item.Id;
                f.Comment = item.Comment;
                CommentList.Add(f);
            }
            

            return CommentList;
        }
        public float Gets(int id)
        {
            var clientResult = new RestClient(BASE_URI);
            var request = new RestRequest("event/show", Method.POST);
            clientResult.AddDefaultHeader("accept", "*/*");

            var obj = new EventViewModel() { id = id };

            request.AddJsonBody(obj);
            var response = clientResult.Execute<EventViewModel>(request);

            return expService.AvgEvent(id);
        }

        // POST: api/EventAPI
        [System.Web.Http.HttpPost]
        public HttpResponseMessage Post()
        { 
              HttpResponseMessage response = new HttpResponseMessage();
              var httpRequest = HttpContext.Current.Request;

              if (httpRequest.Files.Count > 0)
              {
                  foreach (string file in httpRequest.Files)
                  {

                      var postedFile = httpRequest.Files[file];
                      var filePath = HttpContext.Current.Server.MapPath("~/Content/Upload/Event/" + postedFile.FileName);
                      postedFile.SaveAs(filePath);
                  }
              }
              return response;
              }
            /*{
                try
                {
                    var httpRequest = HttpContext.Current.Request;
                    if (httpRequest.Files.Count < 1)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest);
                    }

                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = HttpContext.Current.Server.MapPath("~/Content/Upload/" + postedFile.FileName);
                        postedFile.SaveAs(filePath);
                        // NOTE: To store in memory use postedFile.InputStream
                    }

                    return Request.CreateResponse(HttpStatusCode.Created);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
                }
            }*/


            // PUT: api/EventAPI/5
            public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/EventAPI/5
        public void Delete(int id)
        {
        }
    }
}
