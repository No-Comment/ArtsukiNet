﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ArtsukiServices;
using ArtsukiWeb.Models;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using ArtsukiService;

namespace ArtsukiWeb.Controllers
{
    public class CommunityController : Controller
    {
        private CommunityService communityService = new CommunityService();
        private const string BASE_URI = "http://localhost:18080/tn.esprit.twin.artsuki-web/api/";
        private const string UPLOAD_PATH = "";
        private ArtsukiViewModel connectedUser;

        private string token;

        public void authenticate()
        {
            var client = new RestClient(BASE_URI);
            var request = new RestRequest(Method.POST);
            request.Resource = "authentication";
            var requestObject = new ArtsukiViewModel();
            requestObject.email = "artsuki7@artsuki.com";
            requestObject.password = "753357";
            request.AddJsonBody(requestObject);
            IRestResponse response = client.Execute(request);
            token = response.Content;
        }

        public ArtsukiViewModel getConnectedUser()
        {
            if (token == null)
            {
                authenticate();
            }

            if (connectedUser == null)
            {
                var client = new RestClient(BASE_URI);
                var request = new RestRequest(Method.GET);
                client.ClearHandlers();
                request.Resource = "artsuki";
                request.AddHeader("Authorization", "Bearer " + token);
                var response = client.Execute<ArtsukiViewModel>(request);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    authenticate();
                    getConnectedUser();
                }
                connectedUser = response.Data;
                return connectedUser;
            }
            return connectedUser;
        }
        // GET: Community
        public ActionResult Index()
        {
            var posts = communityService.GetAll();
            var client = new RestClient(BASE_URI);
            var request = new RestRequest("community");
            request.Method = Method.GET;
            request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            var response = client.Execute<List<CommunityViewModel>>(request);
            // ViewBag.ConnectedUser = getConnectedUser();
            // ViewBag.Subscribed = getSubscribedProject();
            return View(response.Data);
        }

        // GET: Community/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Community/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Community/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Community/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Community/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Community/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Community/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
