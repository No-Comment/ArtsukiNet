﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ArtsukiWeb.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProjectApiController : ApiController
    {
        // GET: api/ProjectApi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ProjectApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ProjectApi
        public HttpResponseMessage Post()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count < 1)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/Content/Upload/Shop/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    // NOTE: To store in memory use postedFile.InputStream
                }

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // PUT: api/ProjectApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ProjectApi/5
        public void Delete(int id)
        {
        }
    }
}
