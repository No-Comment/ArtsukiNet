﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtsukiWeb.Models
{
    public class ArtsukiViewModel
    {
        public int id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string nationality { get; set; }
        public string password { get; set; }
        public string phoneNumber { get; set; }
        public string username { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string town { get; set; }
        public string role { get; set; }
        public string biography { get; set; }
        public string art { get; set; }
        public bool verified { get; set; }
    }
}