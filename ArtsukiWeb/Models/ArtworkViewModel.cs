﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtsukiWeb.Models
{
    public class ArtworkViewModel
    {
        public int id { get; set; }
        public long datePub { get; set; }
        public string title { get; set; }
        public string publicationType { get; set; }
        public string category { get; set; }
        public string description { get; set; }
        public string mediaType { get; set; }
        public string mediaPath { get; set; }
        public bool forSale { get; set; }
        public double price { get; set; }
        public ArtsukiViewModel publisher { get; set; } 
    }
}