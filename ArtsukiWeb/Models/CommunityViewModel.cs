﻿using ArtsukiDomain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtsukiWeb.Models
{
    public class CommunityViewModel
    {
        public int id { get; set; }
        public string attachement { get; set; }
        public string message { get; set; }
        public string theme { get; set; }
        //public virtual Publication publication { get; set; }
    }
}