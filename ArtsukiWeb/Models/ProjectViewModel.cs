﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtsukiWeb.Models
{
    public class ProjectViewModel
    {
        public int id { get; set; }
        public string description { get; set; }
        public string photo { get; set; }
        public string title { get; set; }
        [ScaffoldColumn(false)]
        public int numberOfView { get; set; }
        public string category { get; set; }
        public ArtsukiViewModel publisher { get; set; } 
    }
}