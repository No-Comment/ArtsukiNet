﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArtsukiWeb.Models
{
    public class EventViewModel
    {
        public string category { get; set; }

        public string description { get; set; }
        public int nbSeats { get; set; }
        public string photo { get; set; }
        public string places { get; set; }
        public string type { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public ArtsukiViewModel publisher { get; set; }

        public int etat { get; set; }
        
        public String dateEnd { get; set; }
       
        public String dateStart { get; set; }
        
        public String datePub { get; set; }
    


    }
}