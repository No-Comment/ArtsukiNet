﻿using ArtsukiDomain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArtsukiData;
using System.Threading.Tasks;
using ArtsukiData.Infrastructure;

namespace ArtsukiServices
{
    public class ProjectService: Service<Project>, IProjectService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);

        public ProjectService() : base(uow)
        {

        }

        public int GetNumberCollaborators(int id)
        {
            return dbf.DataContext.artsuki.Count(a => a.project1.Any(p => p.id == id));
        }

        public int GetNumberSubscribers(int id)
        {
            return dbf.DataContext.artsuki.Count(a => a.project2.Any(p => p.id == id));
        }
    }
}
