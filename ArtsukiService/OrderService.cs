﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtsukiData.Infrastructure;
using ArtsukiDomain.Entities;
using Service.Pattern;

namespace ArtsukiService
{
    public class OrderService : Service<Order>, IOrderService
    {
        private static  IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);

        public OrderService() : base(uow)
        {
            
        }

        public void AddOrder(int ArtsukiId, int ItemId)
        {
            var Item = dbf.DataContext.Item.Include("Order").Single(i => i.Id == ItemId);
            var Artsuki = dbf.DataContext.artsuki.Single(a => a.id == ArtsukiId);
            var Order = new Order()
            {
                Buyer = Artsuki,
                Amount = Item.Price,
                Item = Item
            };
            dbf.DataContext.Order.AddOrUpdate(Order);
            dbf.DataContext.SaveChanges();
            dbf.DataContext.Database.ExecuteSqlCommand("UPDATE Orders SET Item_Id = " + ItemId + " WHERE Id=" +
                                                       Order.Id);
            dbf.DataContext.Database.ExecuteSqlCommand("UPDATE Items SET Order_Id = " + Order.Id + " WHERE Id=" +
                                                       ItemId);
            dbf.DataContext.Entry(Item).Reload();
        }
    }
}
