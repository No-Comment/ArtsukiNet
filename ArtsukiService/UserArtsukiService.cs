﻿using ArtsukiData.Infrastructure;
using ArtsukiDomain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiService
{
    public class UserArtsukiService : Service<ArtsukiDomain.Entities.Artsuki>, IUserArtsukiService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);
        public UserArtsukiService() : base(uow) {

        }

        public string friendShipStatus(int AsonlineId,int otherAsId)
        {
            IEnumerable<Friendship> efs=uow.getRepository<Friendship>().GetMany(x => ((x.Receiver_id == AsonlineId && x.Requester_id == otherAsId) || (x.Receiver_id == otherAsId && x.Requester_id == AsonlineId))).AsQueryable();
            if(efs.Count() == 0)
            {
                return "notfriends";
            }
            return efs.First().status ;
        }

    }
}
