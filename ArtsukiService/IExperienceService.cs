﻿using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiService
{
   public interface IExperienceService : IService<ArtsukiDomain.Entities.Experience>
    {
    }
}
