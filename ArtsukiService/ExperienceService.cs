﻿using ArtsukiData.Infrastructure;
using ArtsukiDomain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiService
{
    public class ExperienceService : Service<Experience>, IExperienceService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);

        public ExperienceService() : base(uow)
        {

        }
        public List<Experience> getExperienceEvent(int EventId)
        {
            return uow.getRepository<Experience>()
                .GetAll()
                .Where(p => p.EventId == EventId).Where(p=>p.Comment!=null)
                .ToList();
        }
        public List<Experience> getImageEvent(int EventId)
        {
            return uow.getRepository<Experience>()
                .GetAll()
                .Where(p => p.EventId == EventId).Where(p => p.Photo != null)
                .ToList();
        }
        public float AvgEvent(int id)
        {
           int total=uow.getRepository<Experience>().GetAll().Where(f => f.EventId == id).Where(p => p.Rate != 0).Count();

            int sum=uow.getRepository<Experience>().GetAll().Where(f => f.EventId == id).Where(p => p.Rate != 0).Sum(f=>f.Rate);
            if (total != 0) { 
            return sum / total;}
            return 0;
        }
    }
}
