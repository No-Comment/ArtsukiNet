﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtsukiData.Infrastructure;
using ArtsukiDomain.Entities;
using Service.Pattern;

namespace ArtsukiService
{
    public class ArtworkService : Service<Artwork>, IArtworkService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);

        public ArtworkService() : base(uow)
        {
                
        }

        public void updateArtworkToSold(int id)
        {
            dbf.DataContext.Database.ExecuteSqlCommand("UPDATE artwork SET forSale = 0 WHERE id="+id);
        }
    }
}
