﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtsukiDomain.Entities;
using Service.Pattern;

namespace ArtsukiService
{
    public interface IOrderService : IService<Order>
    {
        void AddOrder(int artsukiId, int itemId);
    }
}
