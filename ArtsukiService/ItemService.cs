﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtsukiData.Infrastructure;
using ArtsukiDomain.Entities;
using Service.Pattern;

namespace ArtsukiService
{
    public class ItemService : Service<Item>, IItemService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);

        public ItemService() : base(uow)
        {
                
        }

        public IEnumerable<Item> GetItems()
        {
            return dbf.DataContext.Item.Include("Order").ToList();
        }
    }
}
