﻿using ArtsukiData.Infrastructure;
using ArtsukiDomain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiService
{
   public class EventService : Service<Event>, IEventService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);

        public EventService() : base(uow)
        {

        }
    }
}
