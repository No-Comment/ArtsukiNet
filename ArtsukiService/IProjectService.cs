﻿using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArtsukiDomain.Entities;

namespace ArtsukiServices
{
    public interface IProjectService : IService<ArtsukiDomain.Entities.Project>
    {
        int GetNumberCollaborators(int id);
        int GetNumberSubscribers(int id);
    }
}
