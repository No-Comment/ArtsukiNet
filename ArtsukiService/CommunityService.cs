﻿using ArtsukiDomain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArtsukiData;
using System.Threading.Tasks;
using ArtsukiData.Infrastructure;

namespace ArtsukiService
{
    public class CommunityService: Service<Community>, ICommunityService
    {
        private static IDatabaseFactory dbf = new DatabaseFactory();
        private static IUnitOfWork uow = new UnitOfWork(dbf);

        public CommunityService() : base(uow)
        {

        }

    }
}
