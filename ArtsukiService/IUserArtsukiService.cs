﻿using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtsukiService
{
    interface IUserArtsukiService : IService<ArtsukiDomain.Entities.Artsuki>
    {
        string friendShipStatus(int AsonlineId, int otherAsId);

    }
}
